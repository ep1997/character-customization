﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public AudioSource audioSource;

    public Image audioImage;
    public Sprite audioOn;
    public Sprite audioOff;

    public bool isPaused = false;

	void Start ()
    {
        audioSource = GetComponent<AudioSource>();
	}

    public void AudioButton()
    {
        if(isPaused == false)
        {
            PauseAudio();
            isPaused = true;
        }
        else
        {
            ResumeAudio();
            isPaused = false;
        }
    }

    public void PauseAudio()
    {
        audioSource.Pause();
        audioImage.sprite = audioOff;
    }

    public void ResumeAudio()
    {
        audioSource.Play();
        audioImage.sprite = audioOn;
    }
	
}
