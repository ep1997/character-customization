﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Change : MonoBehaviour
{
    enum AppearanceDetail
    {
        HAIR_MODEL,
        HAIR_COLOR,
        GENDER,
        SKIN_COLOR,
        MALE_CLOTHING,
        CLASS
    }

    [SerializeField] private GameObject[] hairModels;

    [SerializeField] private Transform headAncher;
    [SerializeField] private Transform handAncher;

    [SerializeField] private Color32[] hairColor;

    [SerializeField] private Color32[] skinColor;
    [SerializeField] private Color32[] skinColor_human;
    [SerializeField] private Color32[] skinColor_alien;

    [SerializeField] private GameObject[] bodyModels;

    [SerializeField] private GameObject[] speciesmodels;

    [SerializeField] private GameObject[] classWeapons;


    //[SerializeField] private MeshRenderer hairRenderer;

    GameObject activeHair;
    GameObject activeBody;
    GameObject activeWeapon;


    public int hairIndex = 0;
    public int hairColorIndex = 0;
    public int skinToneIndex = 0;
    public int bodyIndex = 0;
    public int speciesIndex = 0;
    public int classIndex = 0;

    public bool isMale = false;
    public bool isAlien = false;

    private void Start()
    {
        hairIndex = 2;
        bodyIndex = 0;
        skinColor = skinColor_human;
        skinToneIndex = 1;
        classIndex = 0;
        ApplyModification(AppearanceDetail.GENDER, bodyIndex);
        ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
        ApplyModification(AppearanceDetail.HAIR_MODEL, hairIndex);
        ApplyModification(AppearanceDetail.CLASS, classIndex);
    }

    public void HairModelUp()
    {
        if(hairIndex < hairModels.Length -1)
        {
            hairIndex++;
        }
        else
        {
            hairIndex = 0;
        }
        ApplyModification(AppearanceDetail.HAIR_MODEL, hairIndex);
    }

    public void HairModelDown()
    {
        if (hairIndex > 0)
        {
            hairIndex--;
        }
        else
        {
            hairIndex = hairModels.Length - 1;
        }
        ApplyModification(AppearanceDetail.HAIR_MODEL, hairIndex);
    }

    //hair color

    public void HairColorUp()
    {
        if (hairColorIndex < hairColor.Length - 1)
        {
            hairColorIndex++;
        }
        else
        {
            hairColorIndex = 0;
        }
        ApplyModification(AppearanceDetail.HAIR_COLOR, hairColorIndex);
    }

    public void HairColorDown()
    {
        if (hairColorIndex > 0)
        {
            hairColorIndex--;
        }
        else
        {
            hairColorIndex = hairColor.Length - 1;
        }
        ApplyModification(AppearanceDetail.HAIR_COLOR, hairColorIndex);
    }

    //skintone

    public void SkinToneUp()
    {
        if (skinToneIndex < skinColor.Length - 1)
        {
            skinToneIndex++;
        }
        else
        {
            skinToneIndex = 0;
        }
        ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
    }

    public void SkinToneDown()
    {
        if (skinToneIndex > 0)
        {
            skinToneIndex--;
        }
        else
        {
            skinToneIndex = skinColor.Length - 1;
        }
        ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
    }

    //gender

    public void PickFemale()
    {
        isMale = false;

        if (isAlien == true)
        {
            bodyIndex = 2;

            ApplyModification(AppearanceDetail.GENDER, bodyIndex);
            ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
            ApplyModification(AppearanceDetail.HAIR_MODEL, 4);
            ApplyModification(AppearanceDetail.CLASS, classIndex);
        }
        else
        {
            bodyIndex = 0;

            ApplyModification(AppearanceDetail.GENDER, bodyIndex);
            ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
            ApplyModification(AppearanceDetail.HAIR_MODEL, hairIndex);
            ApplyModification(AppearanceDetail.HAIR_COLOR, hairColorIndex);
            ApplyModification(AppearanceDetail.CLASS, classIndex);
        }
    }

    public void PickMale()
    {
        isMale = true;

        if(isAlien == true)
        {
            bodyIndex = 3;

            ApplyModification(AppearanceDetail.GENDER, bodyIndex);
            ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
            ApplyModification(AppearanceDetail.HAIR_MODEL, 4);
            ApplyModification(AppearanceDetail.CLASS, classIndex);
        }
        else
        {
            bodyIndex = 1;

            ApplyModification(AppearanceDetail.GENDER, bodyIndex);
            ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
            ApplyModification(AppearanceDetail.HAIR_MODEL, hairIndex);
            ApplyModification(AppearanceDetail.HAIR_COLOR, hairColorIndex);
            ApplyModification(AppearanceDetail.CLASS, classIndex);
        }

    }

    //species

    public void PickHuman()
    {
        isAlien = false;
        EnableHairButtons();

        if (isMale == true)
        {
            bodyIndex = 1;
        }
        else
        {
            bodyIndex = 0;
        }

        skinColor = skinColor_human;

        ApplyModification(AppearanceDetail.GENDER, bodyIndex);
        ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
        ApplyModification(AppearanceDetail.HAIR_MODEL, hairIndex);
        ApplyModification(AppearanceDetail.HAIR_COLOR, hairColorIndex);
        ApplyModification(AppearanceDetail.CLASS, classIndex);
    }

    public void PickAlien()
    {
        isAlien = true;
        DisableHairButtons();

        if (isMale == true)
        {
            bodyIndex = 3;
        }
        else
        {
            bodyIndex = 2;
        }

        skinColor = skinColor_alien;


        ApplyModification(AppearanceDetail.GENDER, bodyIndex);
        ApplyModification(AppearanceDetail.SKIN_COLOR, skinToneIndex);
        ApplyModification(AppearanceDetail.HAIR_MODEL, 4);
        ApplyModification(AppearanceDetail.CLASS, classIndex);
        //ApplyModification(AppearanceDetail.HAIR_COLOR, hairColorIndex);
    }

    public GameObject[] HairButtons;

    public void DisableHairButtons()
    {
        for( int i = 0; i < HairButtons.Length; i++)
        {
            HairButtons[i].GetComponent<Button>().interactable = false;
            HairButtons[i].GetComponent<Image>().color = new Color32(0, 0, 100, 255);
        }
    }

    public void EnableHairButtons()
    {
        for (int i = 0; i < HairButtons.Length; i++)
        {
            HairButtons[i].GetComponent<Button>().interactable = true;
            HairButtons[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
    }

    public void PickMage()
    {
        classIndex = 0;
        ApplyModification(AppearanceDetail.CLASS, 0);
    }

    public void PickWarrior()
    {
        classIndex = 1;
        ApplyModification(AppearanceDetail.CLASS, 1);
    }

    public Transform dontDestroy;

    void ApplyModification(AppearanceDetail detail, int id)
    {
        switch(detail)
        {
            case AppearanceDetail.HAIR_MODEL:
                if (activeHair != null)
                    GameObject.Destroy(activeHair);

                activeHair = GameObject.Instantiate(hairModels[id]);
                headAncher = activeBody.transform.GetChild(0);
                activeHair.transform.SetParent(headAncher);
                activeHair.transform.ResetTransform();

                ApplyModification(AppearanceDetail.HAIR_COLOR, hairColorIndex);
                break;

            case AppearanceDetail.HAIR_COLOR:
                if(activeHair != null && activeHair.GetComponent<MeshRenderer>() != null)
                activeHair.GetComponent<MeshRenderer>().material.color = hairColor[id];
                break;

            case AppearanceDetail.GENDER:
                if (activeBody != null)
                    GameObject.Destroy(activeBody);

                //activeBody.transform.SetParent(dontDestroy);
                activeBody = Instantiate(bodyModels[id]);
                activeBody.transform.localPosition = new Vector3(-0.6f, -0.44f, 0.23f);
                activeBody.transform.localEulerAngles = Vector3.zero;
                break;

            case AppearanceDetail.CLASS:
                if (activeWeapon != null)
                    GameObject.Destroy(activeWeapon);

                activeWeapon = Instantiate(classWeapons[id]);
                handAncher = activeBody.transform.GetChild(1);
                activeWeapon.transform.SetParent(handAncher);
                activeWeapon.transform.localPosition = handAncher.transform.localPosition;

                //ApplyModification(AppearanceDetail.CLASS, classIndex);
                break;

            case AppearanceDetail.SKIN_COLOR:
                    activeBody.GetComponent<MeshRenderer>().material.color = skinColor[id];
                    headAncher = activeBody.transform.GetChild(0);
                    headAncher.GetComponent<MeshRenderer>().material.color = skinColor[id];
                break;

        }
    }

    public void ChangeClass()
    {

    }
	
}
